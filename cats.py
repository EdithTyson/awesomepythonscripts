#!/usr/bin/python3

try:
    numcats = int(input("How many cats do you have? \n"))
    if numcats >= 4:
        print("That is a lot of cats")
    elif numcats >= 0:
        print("That is not many cats")
    else:
        print("Please enter a positive number")
except ValueError:
    print("you did not enter a number")