#!/usr/bin/python3

def palindrome(teststr): #This function will test if a string is a palindrome
    a = list(teststr)
    if a[::-1] == a:
        print("This is a palindrome!")
    else:
        print("This is not a palindrome")
palindrome(input("Please enter a string to test if it is a palindrome\n"))