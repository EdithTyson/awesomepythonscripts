#!/usr/bin/python3
birthday = {'Edith':'01/01/1970'}
def addbday():
    name = input("What is the name of the birthday you would like to add:\n")
    date = input("Please enter the date of the birthday you would like to add:\n")
    bday = {name:date}
    birthday.update(bday)
    input("Press enter to continue...")
    return

def searchbday():
    name = input("Please enter a name to search:")
    if name in birthday:
        print(birthday[name])
        input("Press enter to continue...")
    else:
        print("We don't have that name in our records.")
        input("Press enter to continue...")
    return
def listall():
    print(birthday)
    input("Press enter to continue...")
    return

while True:
    print("Hello, Please make a selection from the options below")
    print("1. Add a birthday\n2. Search for a birthday\n3. List all entries \n4. Exit")
    selection = int(input())
    if selection == 1:
        addbday()
    if selection == 2:
        searchbday()
    if selection == 3:
        listall()
    if selection == 4:
        exit()